def print_calculations(x, y)
  addition = x + y
  substitution = x - y
  product = x * y
  division = x / y

  bin_x = x.to_s(2)
  bin_y = y.to_s(2)

  oct_x = x.to_s(8)
  oct_y = y.to_s(8)

  hex_x = x.to_s(16)
  hex_y = y.to_s(16)

  puts "x + y = " + addition.to_s
  puts "x - y = " + substitution.to_s
  puts "x * y = " + product.to_s
  puts "x / y = " + division.to_s

  puts "Bin: x = " + bin_x + "; y = " + bin_y
  puts "Oct: x = " + oct_x + "; y = " + oct_y
  puts "Hex: x = " + hex_x + "; y = " + hex_y
end

print "Enter x: "
x = Integer(gets.chomp)

print "Enter y: "
y = Integer(gets.chomp)

print_calculations(x, y)
